import { useState, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";

import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  Form,
  Button,
  Card,
  Col,
  Row,
  Table,
  InputGroup
  
} from "react-bootstrap";

import Swal from "sweetalert2";
import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/esm/styles/hljs";

import request from "../api/requestApi";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlay, faArchive, faSave, faPlusSquare, faAngleRight, faMapSigns} from '@fortawesome/free-solid-svg-icons';

const EditarScenarios = () => {
  const [scenario, setScenario] = useState("");
  const [scenarios, setScenarios] = useState("");
  const [reservesScenario, setReservesScenario] = useState("");
  const [description, setDescription] = useState("");
  const [limit, setLimit] = useState("");
  const [tryed, setTryed] = useState("");
  const [country, setCountry] = useState("");
  const [enableScenario, setEnableScenario] = useState("");
  const [userId, setUserId] = useState("");
  const [user, setUser] = useState("");
  const [queryInfo, setQueryInfo] = useState([]);
  const [btnSave, setBtnSave] = useState(false);
  const [message, setMessage] = useState("");
  const [loadReturnScenarios, setLoadReturnScenarios] = useState(false);
  const [disabledSatusReserve ,setDisabledSatusReserve ] = useState([]);
  const [disabledSatusReserveEnabled ,setDisabledSatusReserveEnabled ] = useState([]);

  const { id } = useParams();
  const history = useHistory();
  const UID = id;

  const getScenarios = async () => {
    const scenarioResult = await request.get(`/scenarios`, {
      params: {
        scenarios: UID,
      },
    });
    return scenarioResult.data[0];
  };

  const getReservation = () => {
   
    let  params = {
      scenarios: UID,
    };
    request.get('/reservations', { data: params }).then(result => { 
      console.log(result);
      setReservesScenario(result.data);
     }).catch(error => {
       console.log(error.code);
     });
  } 

  const moreQuery = () => {
    const objQueryInfo = {
      foreignKeyInNot: "",
      credentials: "",
      foreignKey: "",
      query: "",
      holdField: "",
    };
    let newArr = [...queryInfo];
    newArr.push(objQueryInfo);
    setQueryInfo(newArr);
  };
  const lessQuery = () => {
    let newArr = [...queryInfo];
    newArr.pop();
    setQueryInfo(newArr);
  };

  const updateFieldChanged = (index) => (e) => {
    console.log(queryInfo);
    let newArr = [...queryInfo];
    let name = e.target.name;
    newArr[index][name] = e.target.value;
    setQueryInfo(newArr); // ??
    console.log(queryInfo);
  };

  const objScenarioIsNotNull = (obj) => {
    if (
      !obj.user ||
      !obj.userId ||
      !obj.description ||
      !obj.limit ||
      !obj.try ||
      !obj.enableScenario ||
      !obj.country ||
      !obj.queryInfos
    ) {
      setMessage("there is a blank field");
      return false;
    } else {
      setMessage("");
      return true;
    }
  };

  const save = async (value) => {
    try {
      const objScenario = {
        user: user,
        userId: userId,
        description: description,
        limit: limit,
        try: tryed,
        enableScenario: enableScenario,
        country: country,
        queryInfos: queryInfo,
      };

      if (objScenarioIsNotNull(objScenario) === true) {
        setBtnSave(true);

        if (value === "update") {
          Swal.fire({
            title: "You will update Scenário",
            text: "do you have sure ?",
            confirmButtonText: `ok`,
            showCancelButton: true,
            cancelButtonText: "Cancel",
            closeOnCancel: false,
          })
            .then((result) => {
              if (result.isConfirmed) {
                request
                  .patch(`/scenarios?scenarios=${scenario} `, objScenario)
                  .then((responseRequest) => {
                    console.log("retorno");
                    if (responseRequest.status === 200) {
                      Swal.fire("Good job!", "Save with Success!", "success");
                    } else {
                      Swal.fire("Bad Request!", "Sorry!", "error");
                    }
                  });
              }
            })
            .catch((error) => {
              Swal.fire("Bad Request!", "Sorry!", "error");
            });
        } else if (value === "new") {
          Swal.fire({
            title: "You will created new Scenário",
            text: "do you have sure ?",
            confirmButtonText: `ok`,
            showCancelButton: true,
            cancelButtonText: "Cancel",
            closeOnCancel: false,
          })
            .then(async (result) => {
              if (result.isConfirmed) {
                await request.post(
                  `/scenarios?scenarios=${scenario} `,
                  objScenario
                );
                history.push("/AllScenarios");
              }
            })
            .catch((error) => {
              Swal.fire("Bad Request!", "Sorry!", "error");
              console.log(error);
            });
        }
      }
    } catch (e) {
      console.log(e);
    } finally {
      setBtnSave(false);
    }

    setBtnSave(false);
  };

  const parameterPeru = async () => {
    const objPeru = {
      parameters: [
        { field: "end_cycle", value: "202107" },
        { field: "begin_sub_cycle", value: "202107" },
        { field: "end_sub_cycle", value: "202107" },
        { field: "structure_level_type1", value: "3" },
        { field: "structure_code1", value: "2853" },
        { field: "structure_level_type2", value: "2" },
        { field: "structure_code2", value: "401000069" },
        { field: "structure_level_type3", value: "1" },
        { field: "structure_code3", value: "400001359" },
      ],
    };
    return objPeru;
  };

  const parameterMalasia = async () => {
    const objMalasia = {
      parameters: [
        { field: "end_cycle", value: "202107" },
        { field: "begin_sub_cycle", value: "202107" },
        { field: "end_sub_cycle", value: "202107" },
        { field: "structure_level_type1", value: "1" },
        { field: "structure_code1", value: "1" },
      ],
    };
    return objMalasia;
  };

  const parameterChile = async () => {
    const objChile = {
      parameters: [
        { field: "end_cycle", value: "202110" },
        { field: "begin_sub_cycle", value: "202110" },
        { field: "end_sub_cycle", value: "202110" },
        { field: "structure_level_type1", value: "1" },
        { field: "structure_code1", value: "200002804" },
      ],
    };
    return objChile;
  };

  const parameterDefault = async () => {
    const objDefault = {
      parameters: [
        { field: "end_cycle", value: "202110" },
        { field: "begin_sub_cycle", value: "202110" },
        { field: "end_sub_cycle", value: "202110" },
        { field: "structure_level_type1", value: "1" },
        { field: "structure_code1", value: "1" },
      ],
    };
    return objDefault;
  };

  const executeScenario = async () => {
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Your load could left until 3 min',
      showConfirmButton: false,
      timer: 5000
    })
    //setErrorReturnScenarios(false);
    setLoadReturnScenarios(true);
    //setReturnScenarios("");
    let parameterScenario;
    /* fazer a requisiçao, pegar o pais e depois executar com os parametros
     */
    let responseRequest = await request.get(`/scenarios?scenarios=${UID}`);
    
    if (responseRequest.data[0].country){
      if (responseRequest.data[0].country.toLowerCase().trim() === "peru") {
        parameterScenario = await parameterPeru();
      } else if (
        responseRequest.data[0].country.toLowerCase().trim() === "chile"
      ) {
        console.log("parameter chile");
        parameterScenario = await parameterChile();
      } else if (
        responseRequest.data[0].country.toLowerCase().trim() === "malásia"
      ) {
        parameterScenario = await parameterMalasia();
      } else {
        parameterScenario = await parameterDefault();
      }
    } else {
      parameterScenario = await parameterDefault();
    }
    try {
      responseRequest = await request.post(
        `/scenarios/execution?rows=10&ttl=0&scenarios=${UID}`,
        parameterScenario
      );
      if (responseRequest.data.message) {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Result Not Found',
          showConfirmButton: false,
          timer: 3000
        })
      }
      console.log(responseRequest.data);
      setScenarios(responseRequest.data);
    } catch (err) {
      console.log(err);
      //setErrorReturnScenarios(true);
    } finally {
      setLoadReturnScenarios(false);
    }

    setLoadReturnScenarios(false);
  };


  const reserve = async (row , UID, key) => {

    let keys = Object.keys(row);
    let values = Object.values(row);
    let testData = [];
    for (let index = 0; index < keys.length; index++) {
      testData.push({"field" :keys[index] , "value" : String(values[index])});
    }
    
    const jsonPost = {
      "scenarios": UID,
      "user": "scenario",
      "userId": "1",
      "testData":testData,
      "ttl" : "40000000000"
    };
   
    setDisabledSatusReserve([...disabledSatusReserve, key]);

    await request.post(
      `/reservations`,
      jsonPost
    );
    
  }

  const disabledReserve = async (row , UID, key) => {

    setDisabledSatusReserveEnabled([...disabledSatusReserveEnabled, key]); 
    console.log(row.reservations);

    const jsonPost = {
      "reservations": row.reservations,
      "user": "scnario",
      "userId": "1"
    };
     
    request.delete('/reservations', { data: jsonPost }).then(data => {  })
   
  }

  const GetStart = async () => {
    const scenarioObj = await getScenarios();
    setDescription(scenarioObj.description);
    setUser(scenarioObj.createdBy);
    setUserId(scenarioObj.createdById);
    setDescription(scenarioObj.description);
    setScenario(scenarioObj.scenarios);
    setLimit(scenarioObj.limit);
    setTryed(scenarioObj.try);
    setCountry(scenarioObj.country);
    setEnableScenario(scenarioObj.enableScenario);
    setQueryInfo(scenarioObj.queryInfos);

    getReservation();

  };

  useEffect(() => {
    GetStart();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container className="form">
      <Card.Header>
        <h5 align="center">
          <b><FontAwesomeIcon icon={faMapSigns}/> Title Scenario</b>
        </h5>
      </Card.Header>
      <Card className="mt-3">
        <Card.Body>
          <Row className="">
            <Form.Group className="mb-3" controlId="TitleScenario" as={Col}>
              <Form.Label>
                <b>Scenario</b>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={scenario || ""}
                name="scenario"
                //onChange={(e) => setScenario(e.target.value)}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="Description" as={Col}>
              <Form.Label>
                <b>Description</b>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={description || ""}
                name="description"
                onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>
          </Row>
          <Row className="">
            <Form.Group className="mb-3" controlId="Country" as={Col}>
              <Form.Label>
                <b>Country</b>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={country || ""}
                name="country"
                onChange={(e) => setCountry(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="Limit" as={Col}>
              <Form.Label>
                <b>Limit</b>
              </Form.Label>
              <Form.Control
                type="number"
                placeholder=""
                value={limit || ""}
                name="limit"
                onChange={(e) => setLimit(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="Try" as={Col}>
              <Form.Label>
                <b>Try</b>
              </Form.Label>
              <Form.Control
                type="number"
                placeholder=""
                value={tryed || ""}
                name="tryed"
                onChange={(e) => setTryed(e.target.value)}
              />
            </Form.Group>
          </Row>
          <Row className="">
            <Form.Group className="mb-3" controlId="EnabledScenario" as={Col}>
              <Form.Label>
                <b>Enabled Scenário</b>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={enableScenario || ""}
                name="enableScenario"
                onChange={(e) => setEnableScenario(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="User" as={Col}>
              <Form.Label>
                <b>User</b>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={user || ""}
                name="user"
                onChange={(e) => setUser(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="UserId" as={Col}>
              <Form.Label>
                <b>User ID</b>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder=""
                value={userId || ""}
                name="userId"
                onChange={(e) => setUserId(e.target.value)}
              />
            </Form.Group>
          </Row>
        </Card.Body>
      </Card>
      {queryInfo.length > 0 &&
        queryInfo.map((data, index) => {
          return (
            <div key={index}>
              <br />
              <Card.Header>
                <h5 align="center">
                  <b> <FontAwesomeIcon icon={faAngleRight}/> {' '}Query {index + 1}</b>
                </h5>
              </Card.Header>
              <Card key={index} className="mt-3">
                <Card.Body>
                  <Row className="">
                    <Form.Group
                      as={Col}
                      className="mb-3"
                      controlId={`Credential${index}`}
                    >
                      <Form.Label>
                        <b>Credencial</b>
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder=""
                        value={data.credentials || ""}
                        name="credentials"
                        onChange={updateFieldChanged(index)}
                      />
                    </Form.Group>

                    <Form.Group
                      as={Col}
                      className="mb-3"
                      controlId={`foreignKeyInNot${index}`}
                    >
                      <Form.Label>
                        <b>foreignKeyInNot</b>
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder=""
                        value={data.foreignKeyInNot || ""}
                        name="foreignKeyInNot"
                        onChange={updateFieldChanged(index)}
                      />
                    </Form.Group>
                    <Form.Group
                      as={Col}
                      className="mb-3"
                      controlId={`foreignKey${index}`}
                    >
                      <Form.Label>
                        <b>foreignKey</b>
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder=""
                        value={data.foreignKey || ""}
                        name="foreignKey"
                        onChange={updateFieldChanged(index)}
                      />
                    </Form.Group>
                    <Form.Group
                      as={Col}
                      className="mb-3"
                      controlId={`HoldField${index}`}
                    >
                      <Form.Label>
                        <b>HoldField </b>
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder=""
                        value={data.holdField || ""}
                        name="holdField"
                        onChange={updateFieldChanged(index)}
                      />
                    </Form.Group>
                  </Row>
                  <Form.Group className="mb-3" controlId={`Query${index}`}>
                    <Form.Label>
                      <b>Query</b>
                    </Form.Label>
                    <SyntaxHighlighter language="javascript" style={docco}>
                      {data.query}
                    </SyntaxHighlighter>
                    <Form.Control
                      as="textarea"
                      style={{ height: "200px" }}
                      placeholder=""
                      value={data.query || ""}
                      name="query"
                      onChange={updateFieldChanged(index)}
                    />
                  </Form.Group>
                </Card.Body>
              </Card>
            </div>
          );
        })}

      {message && (
        <div className="alert alert-danger" role="alert">
          <center>{message}</center>
        </div>
      )}

      <div className="container">
        <div className="row justify-content-md-center">
          <div className="col-md-auto">
            <Button
              variant="success"
              className="mt-3 "
              onClick={(e) => save("update")}
              disabled={btnSave}
            >
            <FontAwesomeIcon icon={faSave}/> {' '}
              Save
            </Button>
          </div>
          <div className="col-md-auto">
            <Button
              variant="primary"
              className="mt-3 btn btn-secondary"
              onClick={(e) => moreQuery()}
              disabled={btnSave}
            >
            <FontAwesomeIcon icon={faPlusSquare}/> {' '} More Query
            </Button>
          </div>
          <div className="col-md-auto">
            <Button
              variant="primary"
              className="mt-3 btn btn-secondary"
              onClick={(e) => lessQuery()}
              disabled={btnSave}
            >
              
            <FontAwesomeIcon icon={faPlusSquare}/> {' '}Less Query
            
            </Button>
          </div>
          <div className="col-md-auto">
            <Button
              variant="danger"
              className="mt-3 mb-3"
              onClick={(e) => save("new")}
              disabled={btnSave}
            >
            <FontAwesomeIcon icon={faPlusSquare}/> {' '}
            New
            </Button>
          </div>
        </div>
      </div>
     
      <Card.Header>
        <b>Execute Scenario</b>
      </Card.Header>
      <Card className="mb-3">
        <Card.Body>
          {!loadReturnScenarios ? (
            <Button
              variant="success"
              size="sm"
              onClick={() => executeScenario()}
            >
            <FontAwesomeIcon icon={faPlay}/>
            {' '}  Execute
            </Button>
          ) : (
            <div className="loader"></div>
          )}
        </Card.Body>

        {
          scenarios.length > 0 && ( 
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th width="50px">#</th>
                <th>Type</th>
                <th>Action Reserve</th>
              </tr>
            </thead>
            <tbody>
              {scenarios &&
                scenarios.map((row, key) => (
                  <tr key={key}>
                    <td>{key}</td>
                    <td>{JSON.stringify(row)}</td>
                    <td>
                    <InputGroup className="" size="sm">
                      <Button variant="outline-secondary" id="button-addon2" onClick={() => reserve(row,UID,key)} disabled={ disabledSatusReserve.includes(key) } >
                      <FontAwesomeIcon icon={faArchive}/> Reserve
                      </Button>
                    </InputGroup>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
          )
        }
      </Card>

      <Card.Header>
        <b>Reserves</b>
      </Card.Header>
      <Card className="mb-3">
        {
          reservesScenario.length > 0 && ( 
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th width="50px"> # </th>
                <th> Type </th>
                <th> Action Reserve </th>
              </tr>
            </thead>
            <tbody>
              {reservesScenario &&
                reservesScenario.map((row, key) => (
                  <tr key={key}>
                    <td>{key}</td>
                    <td>{JSON.stringify(row.testData)}</td>
                    <td>
                    <InputGroup className="" size="sm">
                      <Button variant="outline-secondary" id="button-addon2" onClick={() => disabledReserve(row,UID,key)} disabled={ disabledSatusReserveEnabled.includes(key) } >
                      <FontAwesomeIcon icon={faArchive }/> Enabled
                      </Button>
                    </InputGroup>
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
          )
        }
      </Card>
    </Container>
  );
};

export default EditarScenarios;
