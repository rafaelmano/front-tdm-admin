import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import { Form, Table, Container, Col, Row , Button, Card} from "react-bootstrap";

import request from "../api/requestApi";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit,faMapSigns } from '@fortawesome/free-solid-svg-icons'

const AllScenarios = () => {

  const [scenariosCopy, setScenarioCopy] = useState([]);
  const [scenarios, setScenarios] = useState(null);
  const [country, setCountry] = useState(null);
  const [scenario, setScenario] = useState(null);
  const getScenarios = () =>
  request.get(`/scenarios`).then((res) => setScenarios(res.data));

  const filterScenario = (e) => {
    if (scenariosCopy.length === 0) {
      setScenarioCopy(scenarios);
    }
    setScenarios(scenariosCopy);
    let scenariosNew;

    if (scenario) {
      scenariosNew = scenariosCopy.filter((value, i) => {
        return (
          value.description.toUpperCase().indexOf(scenario.toUpperCase()) > -1
        );
      });
    }

    if (country) {
      // eslint-disable-next-line array-callback-return
      scenariosNew = dataAllFilter(scenariosNew).filter((value, i) => {
        if (value.country) {
          return (
            value.country
              .trim()
              .toUpperCase()
              .indexOf(country.trim().toUpperCase()) > -1
          );
        }
      });
    }

    setScenarios(dataAllFilter(scenariosNew));
  };

  const dataAllFilter = (value) => {
    if (!value) {
      return scenariosCopy;
    } else {
      return value;
    }
  };

  useEffect(() => {
    getScenarios();
  }, []);

  return (
    <Container>
      <Card.Header className="mb-3">
        <h5 align="center">
          <b><FontAwesomeIcon icon={faMapSigns}/> Scenarios</b>
        </h5>
      </Card.Header>
      <Form>
        <Row className="mb-3">
          <Form.Group as={Col} controlId="formGridZip">
            <Form.Label><b>Scenario</b></Form.Label>
            <Form.Control
              type="text"
              placeholder=""
              name="scenario"
              onKeyUp={(e) => setScenario(e.target.value)}
              onChange={(e) => filterScenario(e.target.value)}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formGridZip">
            <Form.Label><b>Country</b></Form.Label>
            <Form.Control
              type="text"
              placeholder=""
              name="country"
              onKeyUp={(e) => setCountry(e.target.value)}
              onChange={(e) => filterScenario(e.target.value)}
            />
          </Form.Group>
        </Row>
      </Form>
      <Table striped bordered hover size="sm">
        <thead>
          <tr>
            <th>Description</th>
            <th>Country</th>
            <th>Enable</th>
            <th>UID</th>
            <th colSpan="2">Edit</th>
          </tr>
        </thead>
        <tbody>
          {scenarios &&
            scenarios.map((scenario) => (
              <tr key={scenario.scenarios}>
                <td>{scenario.description}</td>
                <td>{scenario.country}</td>
                <td>{scenario.enableScenario}</td>
                <td>{scenario.scenarios}</td>
                <td>
                  <Link to={`/EditarScenarios/${scenario.scenarios}`}>
                    <Button variant="secondary" size="sm">
                        <FontAwesomeIcon icon={faEdit}/>Edit
                    </Button>
                  </Link>
                  </td>
              </tr>
            ))}
        </tbody>
      </Table>
    </Container>
  );
};

export default AllScenarios;
