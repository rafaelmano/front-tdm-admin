import React from "react";
import AllScenarios from "./pages/allScenarios";
import EditarScenarios from "./pages/editScenarios";


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/allScenarios">Scenarios</Link>
            </li>
           
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
       
          <Switch>
            <Route path="/allScenarios" children={<AllScenarios />} >
            </Route>
            <Route path="/EditarScenarios/:id" children={ <EditarScenarios/>  }>
            </Route>
           
          </Switch>
      </div>
    </Router>
    
  );
}


